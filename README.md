# Arch Linux General

Using i3 window manager, X11 settings.

Alt as primary modifier for i3.

## keybinding Adaptations

Using Xmodmap, the Left Super Key was set as the 'Mode Switch'
```
keycode 133 = Mode_switch Super_L Super_L
```

The 'ijkl' was set as the secondary arrow key set:
```
keycode  31 = i I KP_Up KP_Up
keycode  44 = j J KP_Left KP_Left
keycode  45 = k K KP_Down KP_Down
keycode  46 = l L KP_Right KP_Right
```

Keys are found with:
```
xbindkeys -k
```

## bash Adaptations

Prompt is custom, and there are some aliases near the bottom.
Colours are referenced in a different file.

## xterm Adaptations

Fonts:
```
XTerm*faceName: LiberationMono-Regular:style=Book:antialias=false
XTerm.vt100.faceSize: 11
```

Key-Combos:
```
XTerm.vt100.translations: #override \n\
    Ctrl Shift <Key>C:   copy-selection(CLIPBOARD) \n\
    Ctrl Shift <Key>V:   insert-selection(CLIPBOARD)
    Shift<Btn1Down>:  select-start()
    Shift<Btn1Motion>:  select-extend()
    Shift<Btn1Up>:   select-end(CLIPBOARD)
    Ctrl <Key> minus: smaller-vt-font() \n\
    Ctrl <Key> plus: larger-vt-font() \n\
    Ctrl <Key> 0: set-vt-font(d)
```

Fore and Back Ground colours:
```
XTerm.vt100.reverseVideo: false
XTerm.vt100.foreground: grey
XTerm.vt100.background: black
```

## i3 Adaptations

Alt+Return launches `xterm` instead
```
bindsym Mod1+Return exec xterm
```

Set window title font to `System Sans Francisco`
```
font pango:System San Francisco Display 9
```

Alternative Arrow key bindings `ijkl`

```
set $up i
set $down k
set $left j
set $right l
```

Workspaces
```
set $workspace1 "1: Consoles "
set $workspace2 "2: Browser "
set $workspace3 "3: Editors "
set $workspace4 "4: Code "
set $workspace5 "5: File Browser "
set $workspace6 "6"
set $workspace7 "7"
set $workspace8 "8"
set $workspace9 "9"
set $workspace10 "10: Media "
```

Window colours too.

```

set $bg-colour                    #3F3F3F
set $inactive-bg-colour     #1F1F1F
set $text-colour                  #B9B9C9
set $inactive-text-colour   #676E7D
set $urgent-bg-colour       #DDBBBB
set $active-border              #DDDDDD

#                                            border                           background                  text                                  indicator
client.focused                     $active-border             $bg-colour                    $text-colour                   #111f11
client.focused_inactive     $bg-colour                    $bg-colour                    $inactive-text-colour    #111f11

```

#### Resources
https://fontawesome.com/cheatsheet

## Keybinding

#### Resources
https://wiki.archlinux.org/index.php/xmodmap
